# How to install and run the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework12.git`
2. Run `docker-compose up -d load-balancer` in the project folder
3. Open http://localhost/ in your web browser to test load balancer
